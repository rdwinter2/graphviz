FROM ubuntu:latest

RUN apt-get update && apt-get install -y \
  apt-utils \
  strace \
  graphviz \
  graphviz-doc \
  fonts-liberation \
  gsfonts \
  && rm -rf /var/lib/apt/lists/*
